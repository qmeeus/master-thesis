#!/bin/bash

pdftk kulemt_1.pdf burst output kulemt_%02d.pdf
pdftk kulemt_01.pdf kulemt_02.pdf thesis.pdf kulemt_03.pdf output intermediary.pdf
rm kulemt_0*.pdf
pdftk thesis.pdf dump_data output original.txt
pdftk intermediary.pdf dump_data output updated.txt

cat original.txt | grep "^Info" > new.txt
cat updated.txt | grep -v "Info" >> new.txt
pdftk intermediary.pdf update_info new.txt output final.pdf
rm original.txt updated.txt new.txt doc_data.txt intermediary.pdf

