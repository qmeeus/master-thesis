# LaTeX Makefile v0.33 -- LaTeX only

MAIN=thesis.tex  # set the path to your TeX file here
SHELL=/bin/bash   # for the while loop below

all:  ## Compile paper
	rubber --pdf $(MAIN)

clean:  ## Clean output files
	rubber --clean $(MAIN)

watch:  ## Recompile on updates to the source file
	@echo "Night gathers, and now my watch begins."
	@while true; do ls -d */* | entr -d -s "sleep 1.0; make all"; done
