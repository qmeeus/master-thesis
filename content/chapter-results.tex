\chapter{Results and Discussion}
\label{sec:results}

% \cleanchapterquote{Users do not care about what is inside the box, as long as the box does what they need done.}{Jef Raskin}{about Human Computer Interfaces}

\section{Semi-supervised Classification Scores}
\label{sec:results:classifiers}
\subsection{Comparison with supervised models}
\label{sec:results:classifiers:ss-vs-sup}
\begin{wrapfigure}{r}{0.55\textwidth}
    \begin{subfigure}{\linewidth}
        \vspace{-15pt}
        \hspace{-12pt}
        \centering
        \includegraphics[width=\linewidth]{figures/f1score_comparison.eps}
        \vspace{-6pt}
        \caption{Comparison of the f1 scores obtained on the best models on the test set (averaged over 3 runs)}
        \label{fig:f1-comparison}
        \vspace{11pt}
    \end{subfigure}
    \begin{subfigure}{\linewidth}
        \centering
        \resizebox{0.8\linewidth}{!}{%
            \begin{tabular}{lrrr}
                \toprule
                {} &  Average &  StDev &  Rank \\
                \midrule
                TF-IDF + LR          &   0.8319 & 0.0000 &     8 \\
                CNN-rand             &   0.7693 & 0.0061 &    14 \\
                CNN-non-static       &   0.8215 & 0.0052 &     9 \\
                LSTM                 &   0.7543 & 0.0172 &    16 \\
                Bi-LSTM              &   0.7318 & 0.0185 &    18 \\
                PV-DBOW              &   0.7436 & 0.0018 &    17 \\
                PTE                  &   0.7674 & 0.0029 &    15 \\
                fastText             &   0.7938 & 0.0030 &    13 \\
                fastText (bigrams)   &   0.7967 & 0.0029 &    12 \\
                SWEM                 &   0.8516 & 0.0029 &     2 \\
                LEAM                 &   0.8191 & 0.0024 &    10 \\
                Graph-CNN-C          &   0.8142 & 0.0032 &    11 \\
                Text GCN             &   0.8634 & 0.0009 &     1 \\
                \midrule
                CAE (semi-sup.)      &   0.8457 & 0.0004 &     3 \\
                KATE (semi-sup.)     &   0.8436 & 0.0009 &     4 \\
                K-sparse (semi-sup.) &   0.8428 & 0.0003 &     5 \\
                SSAE (semi-sup.)     &   0.8428 & 0.0012 &     6 \\
                FC classifier        &   0.8405 & 0.0044 &     7 \\
                \bottomrule
            \end{tabular}}
        \caption{Test accuracy on 20Newsgroup dataset of various models including ours}
        \label{table:clf-scores}
    \end{subfigure}
    % \vspace{-11pt}
    \captionsetup{justification=centering}
    \caption{Classification performances}
    \label{fig:clf-scores}
    \vspace{-11pt}
\end{wrapfigure}
{Compared to the fully supervised models, the semi-supervised classifiers perform better when the number of labels are small. In Figure \ref{fig:f1-comparison}, we see that the difference between the test scores gets smaller as more labels are included in the training data. When trained on 20 labels, the semi-supervised autoencoder comes out first and when 200 labels are available, KATE yields better scores. When the threshold of 100 labels per class is reached, all semi-supervised models level-off with the supervised classifier. This suggests that the advantage provided by the unsupervised objective becomes negligible when the classifier is trained on more data (1,000 labels per class corresponds to all available labels). The same observation was reached by \citet{Chapelle:2010} that when we have a lot of labelled data, the unlabelled data does not help nearly as much. Yet, \citet{Chapelle:2010} displays scores slightly better with the EM algorithm for small amounts of labelled data (about 35\% accuracy for 1 label per class versus 31\% accuracy on average for our best model, with a f1-score of 28\%). We reach similar results as \citet{Chapelle:2010} for superior thresholds. Table \ref{table:clf-scores} shows our results for the models trained with all the labels (the last 5 rows) compared to previously published models (taken from \citet{Yao:2018}). TFIDF-LR refers to a logistic regression model with tf-idf features and performs particularly well on long documents. CNN-rand and CNN-non-static \cite{Kim:2014} refer to convolutional neural network with word embeddings initialised randomly or with GloVE word vectors respectively. The LSTM models \cite{Liu:2016} fail to model large sequence, as discussed in previous sections. PV-DBOW \cite{Mikolov:2014} is the distributed bag-of-words version of the paragraph vector model and PTE \cite{Tang:2015} refers to predictive text embedding, all two are presented in Section \ref{sec:background:nlp:representations-learning}. fastText and fastText (bigrams) \cite{Joulin:2016} use averaged word embeddings as document embeddings. SWEM \cite{Shen:2018} performs pooling operations on word embeddings and LEAM \cite{Wang:2018} embed words and labels in a single vector space. Graph-CNN-C \cite{Defferrard:2016} is a model that performs convolutions over word embedding similarity graph. Finally, Text-GCN \cite{Yao:2018} uses convolutional networks on graphs which contains word nodes and document nodes, allowing them to model word co-occurrences in an efficient way. Although we do not reach the best scores, we range 3rd in this ranking, after Text-GCN and SWEM. Given that our models are much less complex and faster to train, these results are still impressive.

\subsection{Analysis of predictions}
\label{sec:results:classifiers:predictions}
Figures \ref{fig:cm1} and \ref{fig:cm100} show the predictions of the best semi-supervised autoencoders (SSAE and KATE) after being trained on 1 and 100 labels. Correct classifications are visible on the diagonal of the matrix and misclassifications are depicted outside of the diagonal. Naturally, as the amount of available supervised data increases, the model makes fewer mistakes. When one unique example per class is used during training, only 34.9\% of the test examples are classified correctly. However, a quick analysis of the frequent errors shows that the classes that are confused with each other often share some overlap. For example, the classes about technical subjects (comp.graphics, comp.os.ms-windows.misc, comp.sys.ibm.pc.hardware, comp.sys.mac.hardware, comp.windows.x, sci.crypt and sci.electronics) form one group where predictions are often attributed to other classes from the same group. Similarly, the topics about religion or atheism are often confused as well and hockey and baseball all end up classified in the class hockey. Even though the amount of errors decreases considerably when more labelled examples are available from each class, the few remaining misclassifications still occur between the same classes. This is due to the fact that the word families that often appear in one class are also frequently used in similar classes and since we force the model to summarise the information it receives, the differences between classes may become less clear.

\begin{figure}
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/confusion_matrix_1.png}
        \caption{Predictions of the best SSAE trained on 1 label/class}
        \label{fig:cm1}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/confusion_matrix_100.png}
        \caption{Predictions of the best KATE trained on 100 labels/class}
        \label{fig:cm100}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/cluster_matrix_10.png}
        \caption{Cluster assignment of best k-sparse trained on 10 labels/class}
        \label{fig:cm10}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/cluster_matrix_1000.png}
        \caption{Cluster assignment of best CAE trained on 1000 labels/class}
        \label{fig:cm1000}
    \end{subfigure}
    \captionsetup{justification=centering}
    \caption{Confusion matrices and cluster assignments of the different models trained on various thresholds of labelled data}
    \label{fig:results-matrices}
\end{figure}

\section{Semi-supervised Representation Learning}
\label{sec:results:autoencoders}
In this section, we compare the results obtained on the models trained in an unsupervised setting with the autoencoders that incorporate a supervised objective. All four models can be trained in an unsupervised way if we do not connect a classifier layer to the output of the encoder (or if we set the parameter $\alpha$ to zero). To compare the quality of the encodings, we use the learned representations as input to other models and compare their performances. In a first step, we train a support vector classifier to predict the class labels on the test set. Next, we train a $k$-means algorithm to cluster the input data and compare the resulting clusters with the class labels.
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/unsup_scores.pdf}
    \vspace{-16pt}
    \caption{Comparison of the encodings learned by unsupervised and semi-supervised models with various thresholds on classification (SVM) and clustering tasks ($k$-means): the points represent the mean score obtained by one model trained with a specific number of labelled examples and the error bars show the standard deviation around the mean. The classification power of the encoding is measured by calculating the f1-score of a linear SVM trained with 5-fold CV. The clustering accuracy is the proportion of examples that were assigned to a correct cluster, that is, a cluster assigned to the correct target after running a linear assignment algorithm (Hungarian algorithm).}
    \label{fig:unsup-scores}
    \vspace{-16pt}
\end{figure}

\subsection{Support Vector Classifier}
\label{sec:results:autoencoders:svm}
On Figure \ref{fig:unsup-scores}, we observe that none of the unsupervised models manage to create representations that allow the SVM to linearly separate the examples. Although the unsupervised K-competitive autoencoder demonstrates slightly better results, it does not come close to the semi-supervised models, even when they are trained with only one label per class (20 labels in total). This is in line with the results of \citet{Chen:2017}, with the exception of their model and the k-sparse autoencoders. They were able to show that a fully connected classifier trained on their model's representations performed considerably better, achieving a 76\% accuracy on the test set. However, we were not able to reproduce these results. Among the semi-supervised autoencoders, KATE clearly stands out as it achieves 64.91\% f1-score (averaged over 3 runs) even though it was trained with only $20$ labels. As the amount of available labelled examples increases, the difference between the scores gets smaller and disappears completely when all labels are included in the training set. Again, the CAE shows the worst performances, both when trained in an unsupervised and semi-supervised way, whereas the SSAE proves to be more efficient when labels are scarce. 

\subsection{$k$-means}
\label{sec:results:autoencoders:k-means}
We observe the cluster assignment accuracy of the unsupervised and semi-supervised models on the y-axis of Figure \ref{fig:unsup-scores}. Again, except for the unsupervised K-competitive autoencoder, the unsupervised models do not learn representations that can successfully be clustered by $k$-means algorithm. Unsupervised KATE proves to be a valid competitor as it shows better results than the representations learned in a semi-supervised setting when trained on limited number of labels. As more labels are included, the models show similar performances, with a slight advantage for KATE until the threshold of 100 labels per class is reached. Figures \ref{fig:cm10} and \ref{fig:cm1000} show the cluster assignments of the test examples by the k-sparse autoencoder and CAE after being trained on respectively 10 and all labelled examples. We see that even after being trained with all available labels, one cluster (\#9) is ill-defined and some confusion exists between similar classes. Indeed, the class "talk.politics.misc" is either assign to cluster 15 or to cluster 12, together with the class "talk.politics.guns". A similar observation can be made regarding topics about religion or technology. By contrast, model trained on only 200 labels such as shown in Figure \ref{fig:cm10} is only partially able to create encodings that can easily be separated into clusters. Nonetheless, we see that even with a small amount of labels, the model is able to generate encoding that bring similar examples together while distancing examples that belong far apart.  

\section{Exploration of the Network Parameters}
\label{sec:results:parameters}
In this section, we focus on KATE. In particular, we explore the parameters that the network has learned when trained with the smallest amount of labelled data possible (one labelled example per class or 20 labelled examples). As a first step, we explore how the encodings are created by looking at the word composition in the encoding units. Next, we see how the encoded inputs relate to the classes and if comparable documents are represented in a similar way. Inversely, we also verify that documents covering different topics have distinct representations.

\subsection{Topic compositions}
\label{sec:results:parameters:topics}
\begin{table}
    \centering
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{ll|p{10cm}}
            \toprule
            \textbf{Topic} & \textbf{Most likely target class} & \multicolumn{1}{c}{\textbf{Top words}} \\
            \midrule
            \textbf{0} & comp.sys.mac.hardware & lc pc modem simm mb disk port ram mhz screen dresden driver vga serial font meg hardwar mous centri cach \\
            \textbf{1} & rec.sport.baseball & player team hockey season pitch basebal helmet leagu net hitter big tu expo case hand shoei nhl cub rm buy \\
            \textbf{4} & talk.religion.misc & tu de christ armenian atho dresden biblic medic sin stratu valu assum graphic pixmap open religion beck faith religi appressian \\
            \textbf{10} & sci.electronics & de armenian chip signal clipper circuit output design radar orbit light henri ford speed input mi small hp widget mile \\
            \textbf{22} & sci.crypt & crypto clipper escrow sunset sternlight sunris nsa eisa isa bubblejet ax chip deskjet ide vlb metzger pmetzger keyseach drexel bj \\
            \textbf{23} & rec.autos & bike ride tu hp bmw honda mous cool dog nec motorcycl informatik dresden lc wheel bnr rear ground printer mph \\
            \textbf{24} & comp.os.ms-windows.misc & window file printer pc screen simm mous font port mb driver vga directori lc bit meg motif interfac comp disk \\
            \textbf{47} & talk.politics.misc & repli nasa gov problem orbit space drive scienc david bike email window ibm compani launch hole ysu flight fax de \\
            \textbf{49} & sci.space & state gatech orbit support de launch prism tax pitch hitter exist thoma armenian cmu made ac brave season henri helmet \\
            \textbf{79} & comp.graphics & widget tu de dresden xterm motif font pixel pixmap lc graphic pov inf button patch gif sunris beck sunset output \\
            \textbf{81} & alt.atheism & christ faith gld sin lord religion sgi vb vice ico dwyer wpd atho beauchain rushdi agnost risc solntz timmon gregg \\
            \textbf{84} & sci.med & de widget gov diseas tu patient imag stratu nasa water orbit medic medicin graphic book mous nec lc treatment scientif \\
            \textbf{85} & rec.motorcycles & bike zx dresden lafibm behanna uv inf biker beck vb pettefar mcmaster patch quartz maidenhead yfn lafayett stankowitz prism jeep \\
            \textbf{103} & misc.forsale & armenian institut gatech fax soviet screen prism tape ohanu extermin appressian intern sahak dept memori sell melkonian imag sera upenn \\
            \textbf{113} & talk.politics.mideast & mike bike chip clipper ride mail hp colleg team sell season contact dept king driver radio cost blue robert size \\
            \textbf{114} & talk.politics.guns & gun pitch gatech american bill batf brave firearm atf tax atlanta columbia weapon hitter stratu udel cub je staff prism \\
            \textbf{119} & comp.windows.x & tu dresden sunris pixmap question directori app sunset de window font au file pov inf read motif beck andr drexel \\
            \textbf{120} & soc.religion.christian & moral armenian christ rutger fact atho homosexu human children sin religion biblic faith religi book cramer frank polit keith belief \\
            \textbf{121} & comp.sys.ibm.pc.hardware & pc window disk screen drive modem chip mb ms port ram format simm vga printer hardwar memori scsi widget lc \\
            \textbf{127} & rec.sport.hockey & season team pitch philli hitter leagu hockey louisvil dl cub upenn espn sell sunris toyota mike nhl player sunset vb \\
            \bottomrule
        \end{tabular}
    }
    \vspace{11pt}
    \captionsetup{justification=centering}
    \caption{Top words for the topics with the highest predictive scores with regard to the target class (KATE trained on 20 labelled examples)}
    \label{table:topics}
\end{table}
Table \ref{table:topics} shows for each target class the topic that is most predictive for this class and the 20 words that are most strongly linked to this topic, \textit{i.e.} the word composition of the topic. Although we directly see that some topics make perfect sense, other topics are composed of words from multiple semantic families that should not end up together. Further, one word can appear in multiple topics and it is common to observe the same word strongly related to multiple topics. For example, the word ``armenian'' appears in different topics that do not relate to each other (topics 4, 10, 103 and 120). Some topics show a strong similarity, which raises some doubts about the capacity of the model to learn different topics for each of the hidden units.

\subsection{Representation similarities}
\label{sec:results:parameters:classes}
\begin{figure}
    \centering
    \begin{subfigure}{0.8\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/encoding_class_similarities.eps}
        \caption{Average pairwise cosine similarity of the encoded test examples between and inside the different target classes. The encodings were produced by by KATE trained with 20 (0.22\%) labelled examples}
        \label{fig:topic-class-sim}
    \end{subfigure}
    \begin{subfigure}{0.7\textwidth}
        \centering
        \includegraphics[width=\linewidth]{figures/similarity_overlap.pdf}
        \caption{Overlap between intra- and interclass similarities of the encodings generated by KATE at different label thresholds}
        \label{fig:sim-overlap}
    \end{subfigure}
    % \begin{subfigure}{0.45\textwidth}
    %     \centering
    %     \includegraphics[width=\linewidth]{figures/encoding_predictive_power.eps}
    %     \caption{Predictiveness of the encoding units on each of the classes}
    %     \label{fig:encoding-predictiveness}
    % \end{subfigure}
    \captionsetup{justification=centering}
    \caption{Cosine similarities between examples from the test set}
    \label{fig:encoding-sim}
\end{figure}
In order to verify that the model produce similar encodings for documents that share similar topics, we have encoded the test examples using KATE's encoder, after being trained on a dataset with 20 labelled examples. Next, we compute the cosine similarity between each pair of encoded documents, and we average the score for each pair of target class. The result is displayed in Figure \ref{fig:topic-class-sim}. This time again, we see that the documents that cover analogous topics yield comparable encodings and the average similarity is almost systematically larger inside classes. Figure \ref{fig:encoding-sim} shows the overlap between the intra- and inter-class similarities. The interclass similarity (orange curve) should ideally cover a large portion of the x-axis and have a small height, which denote of large differences in between the representations from different classes. The inter-class similarity should cover a small portion of the x-axis and be centered on a value close to 1. For the unsupervised model, all similarities range between 0.99 and 1, and the two distributions overlap, which means that some examples from different classes are more similar than examples from the same class, and all encodings are very similar to one another. As the number of labels increases, the overall similarity moves away from 1, but the relative standard deviation increases for the inter-class similarity and decreases for the intra-class similarity. When there is no overlap between the two curves, this means that the examples from the same class are consistently more similar than examples from different classes.